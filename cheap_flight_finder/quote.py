import datetime


__author__ = "Gianluca Rossi"
__email__ = "gr.gianlucarossi@gmail.com"
__copyright__ = "GNU GPLv3 Licence, 2016"
__maintainer__ = "Gianluca Rossi"


class Quote:
    """The quote for a given combination of outbound and inbound flights"""
    def __init__(self, outbound_date, inbound_date, origin, destination,
                 currency, price):
        assert isinstance(price, float)

        outbound_date = datetime.datetime.strptime(outbound_date, "%Y-%m-%d").date()
        inbound_date = datetime.datetime.strptime(inbound_date, "%Y-%m-%d").date()

        assert outbound_date < inbound_date

        self.outbound_date = outbound_date
        self.inbound_date = inbound_date
        self.origin = origin
        self.destination = destination
        self.currency = currency
        self.price = price

