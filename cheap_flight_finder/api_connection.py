import sys
import httplib2
from urllib.parse import urlencode


__author__ = "Gianluca Rossi"
__maintainer__ = "Gianluca Rossi"
__email__ = "gr.gianlucarossi@gmail.com"
__copyright__ = "GNU GPLv3 Licence, 2016"


class Connection(object):
    """Connect to Skyscanner Business API and retrieve quotes"""

    API_HOST = "http://partners.api.skyscanner.net"
    PRICING_SESSION_URL = "{api_host}/apiservices/pricing/v1.0".format(
        api_host=API_HOST)
    SESSION_HEADERS = {"Content-Type": "application/x-www-form-urlencoded",
                       "Accept": "application/json"}

    def __init__(self, api_key="prtl6749387986743898559646983194", body=None):
        """
        :param api_key: the API key, the default is an API key provided for
        testing purposes in the Skyscanner API documentation
        :param body: the details of the flight we are interested on
        """

        if not api_key:
            raise ValueError("The API key must be provided")

        self.api_key = api_key
        self.body = body
        self.polling_url = None

        self.create_session(api_key=self.api_key, body=self.body)
        self.get_result(polling_url=self.polling_url, api_key=self.api_key)

    def get_result(self, polling_url=None, api_key=None):
        """Download results from API"""

        if not any((polling_url, api_key)):
            raise ValueError("Both polling URL and API key must be provided")

        url = "?apikey=".join((polling_url, api_key))
        return self.make_request(url, body=self.body)

    def make_request(self, service_url, method="GET", headers=None, body=None):
        """Perform either a POST or GET request

        :param service_url: URL to request
        :param method: request method, default is GET
        :param headers: request headers
        :param data: the body of the request
        """

        if "apikey" not in service_url.lower():
            body.update({
                "apikey": self.api_key
            })

        h = httplib2.Http(".cache")
        response, content = h.request(service_url,
                                      method=method,
                                      body=urlencode(body),
                                      headers=headers)
        self.polling_url = response["location"]
        print(str(response))

    def create_session(self, api_key, body):
        """Create the Live Pricing Service session"""

        return self.make_request(self.PRICING_SESSION_URL,
                                 method="POST",
                                 headers=self.SESSION_HEADERS,
                                 body=body)

def main():
    body = {
        "country": "UK",
        "currency": "GBP",
        "locale": "en-GB",
        "originplace": "LOND-sky",
        "destinationplace": "NYCA-sky",
        "outbounddate": "2016-05-01",
        "inbounddate": "2016-05-10",
        "adults": 1,
        "grouppricing": False
    }

    Connection(body=body)


if __name__ == "__main__":
    sys.exit(main())

