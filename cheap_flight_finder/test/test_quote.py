from nose.tools import assert_equal, assert_raises, raises
from cheap_flight_finder.quote import Quote, QuotesCollection


__author__ = "Gianluca Rossi"
__email__ = "gr.gianlucarossi@gmail.com"
__copyright__ = "GNU GPLv3 Licence, 2016"
__maintainer__ = "Gianluca Rossi"


class TestQuote(object):
    """Test flight quotes are correct"""

    outbound_date = "2016-01-01",
    inbound_date = "2016-01-05",
    origin = "LOND-sky",
    destination = "NYCA-sky",
    price = 520.00,
    currency = "GBP",
    market = "en-GB"

    def test_inbound_before_outbound_date(self):
        """Inbound date must me greater or equal than the outbound date"""
        with assert_raises(AssertionError):
            outbound_date_wrong = "2016-01-15"
            Quote(outbound_date=outbound_date_wrong,
                  inbound_date=self.inbound_date,
                  origin=self.origin,
                  destination=self.destination,
                  currency=self.currency,
                  price=self.price)


    def test_price_string(self):
        """Price should not be a string"""
        with assert_raises(AssertionError):
            price_string = "500"
            Quote(outbound_date=self.outbound_date,
                  inbound_date=self.inbound_date,
                  origin=self.origin,
                  destination=self.destination,
                  currency=self.currency,
                  price=price_string)

class TestQuotesCollections(object):
    """Test operations with collections of Quote objects"""

    @classmethod
    def setup_class(cls):
        """This method is run once for each class before any test"""
        quote1 = Quote(outbound_date = "2016-01-01",
                      inbound_date = "2016-01-05",
                      origin = "LOND-sky",
                      destination = "NYCA-sky",
                      price = 520.00,
                      currency = "GBP")

        quote2 = Quote(outbound_date = "2016-01-01",
                      inbound_date = "2016-01-05",
                      origin = "LOND-sky",
                      destination = "NYCA-sky",
                      price = 720.00,
                      currency = "GBP")

        quote3 = Quote(outbound_date = "2016-01-01",
                      inbound_date = "2016-01-05",
                      origin = "LOND-sky",
                      destination = "NYCA-sky",
                      price = 640.00,
                      currency = "GBP")

        quotes = QuotesCollection(quote1, quote2, quote3)

    def test_find_min(self):
        """`find_cheapest()` should return the cheapest flight"""
        assert_equal(quotes.find_cheapest().price, quote1.price)

