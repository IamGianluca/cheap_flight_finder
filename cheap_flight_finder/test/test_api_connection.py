from unittest.mock import MagicMock
from nose.tools import assert_equal
from src.main.api_connector import Connection


__author__ = "Gianluca Rossi"
__email__ = "gr.gianlucarossi@gmail.com"
__copyright__ = "GNU GPLv3 Licence, 2016"
__maintainer__ = "Gianluca Rossi"


class TestAPIConnection(object):
    """Test connection to third party API to retrieve the quotes"""

    def test_api_connection(self):
        """Mock connection to API"""
        headers = {}
        with patch.object(api_connector.APIConnection, "_create_session",
                          return_value=headers) as api_response:
            connection = APIConnection()
            connection._create_session(1, 2, 3)

    def test_empy_response(self):
        """An EmptyResponse exception should be throw when the API returns an
        empty response"""
        pass

