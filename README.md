# A Simple Tool That Notifies You Of The Current Flight Prices To A Given Destination
The purpose of this tool is to query, every time the program is launched, the Skyscanner API and print out the best
prices for the destination defined by the user. 

# To-Do
Some features we want to add in the coming future:

* Relative expensiveness compared to the last 30-day data
* Predictions of price on the next 7, 14 and 21 days
* ...

# Credits
In ~~no~~ particular order of importance:

* My girlfriend, for living 5576 km away from where I live. You see where the idea behind this tool came from
* My friend [Lukasz](https://github.com/lukhar) for not wasting a chance to remind me how cool is Python
* The Python community for making freely available tons of material and code you can learn from
* Skyscanner, Ltd for making freely available their [API](http://business.skyscanner.net/portal/en-GB/Documentation/ApiOverview) 
(up to a certain number of queries per day)

Please feel free to send a Pull Request or fork the repository if you see room for improvements, or simply want to
add some new cool feature. I made this project public because I like to collaborate and learn from others.

# License
GNU GPLv3 Licence, 2016
